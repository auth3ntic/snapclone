//
//  ContainerView.swift
//  Snap
//
//  Created by William Robersone on 8/26/19.
//  Copyright © 2019 William Robersone. All rights reserved.
//

import UIKit

class ContainerView: UIViewController {

    @IBOutlet weak var scroll: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let left = self.storyboard?.instantiateViewController(withIdentifier: "left") as! LeftView
        self.addChild(left)
        self.scroll.addSubview(left.view)
        self.didMove(toParent: self)
        
        let middle = self.storyboard?.instantiateViewController(withIdentifier: "middle") as! MiddleView
        self.addChild(middle)
        self.scroll.addSubview(middle.view)
        self.didMove(toParent: self)
        
        var middleFrame: CGRect = middle.view.frame
        middleFrame.origin.x = self.view.frame.width
        middle.view.frame = middleFrame
        

        let right = self.storyboard?.instantiateViewController(withIdentifier: "right") as! RightView
        self.addChild(right)
        self.scroll.addSubview(right.view)
        self.didMove(toParent: self)
        
        var rightFrame: CGRect = right.view.frame
        rightFrame.origin.x = 2 * self.view.frame.width
        right.view.frame = rightFrame

        self.scroll.contentSize = CGSize(width: (self.view.frame.width) * 3, height: (self.view.frame.height))
    }
    

}
